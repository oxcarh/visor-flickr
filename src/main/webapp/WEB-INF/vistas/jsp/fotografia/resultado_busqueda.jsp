<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="contextPath" value="${pageContext.servletContext.contextPath}" />
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Visor Flickr</title>

    <link rel="stylesheet" href="${contextPath}/recursos/css/normalize.css">
    <link rel="stylesheet" href="${contextPath}/recursos/css/foundation.min.css">
    <link rel="stylesheet" href="${contextPath}/recursos/css/indigo.css">
</head>
<body>

<div class="row" id="contenedor-principal">
    <form action="${contextPath}/fotografia/buscar" method="POST">
        <div class="large-12 columns">
            <h4>Resultado de la busqueda para: ${formularioBusqueda.palabra}</h4>
        </div>
        <div class="large-12 columns">
            <c:forEach var="fotografia" items="${fotografias}">
                <div class="row">
                    <div class="large-2 columns">
                        <img src="${fotografia.url}" alt=""/>
                    </div>
                    <div class="large-10 columns">
                        ${fotografia.id} : ${fotografia.titulo}
                    </div>
                </div>
            </c:forEach>
        </div>
    </form>
</div>

</body>
</html>