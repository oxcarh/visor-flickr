package com.dsindigo.visorflickr.controlador;

import com.dsindigo.visorflickr.servicio.FlikrServicio;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.dsindigo.visorflickr.modelo.FormularioBusqueda;

import javax.annotation.Resource;

@Controller
@RequestMapping( "/fotografia*" )
public class FotografiaController {

	private final Logger log = LoggerFactory.getLogger( getClass() );

	@Resource
	FlikrServicio flikrServicio;

	@RequestMapping( value = "", method = RequestMethod.GET )
	public String index() {
		log.info( "Fotografia: Inicio" );
		// return "redirect:inicio";
		return "inicio";
	}

	@RequestMapping( value = "buscar", method = RequestMethod.POST )
	public String buscar( @ModelAttribute FormularioBusqueda formularioBusqueda, Model model ) {
		model.addAttribute( "formularioBusqueda", formularioBusqueda );
		model.addAttribute( "fotografias", flikrServicio.buscarFotografias( formularioBusqueda.getPalabra() ) );
		return "fotografia/resultado_busqueda";
	}



}
