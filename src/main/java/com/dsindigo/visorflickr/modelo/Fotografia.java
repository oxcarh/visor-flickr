package com.dsindigo.visorflickr.modelo;

public class Fotografia {

	private String id;
	private String url;
	private String titulo;
	private String secreto;

	public String getUrl() {
		return url;
	}

	public void setUrl( String url ) {
		this.url = url;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo( String titulo ) {
		this.titulo = titulo;
	}

	public String getId() {
		return id;
	}

	public void setId( String id ) {
		this.id = id;
	}

	public String getSecreto() {
		return secreto;
	}

	public void setSecreto( String secreto ) {
		this.secreto = secreto;
	}
}
