package com.dsindigo.visorflickr.servicio.impl;

import com.dsindigo.visorflickr.modelo.Fotografia;
import com.dsindigo.visorflickr.servicio.FlikrServicio;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service( "flickrServicio" )
public class FlickrServicioImpl implements FlikrServicio {

	private final Logger log = LoggerFactory.getLogger( FlikrServicio.class );

	@Value( "#{ configuracionProperties['flickr.apiKey'] }" )
	private String apiKey;

	@Value( "#{ configuracionProperties['flickr.api.url'] }" )
	private String apiUrl;

	@Value( "#{ configuracionProperties['flickr.api.metodo.buqueda'] }" )
	private String metodoBusqueda;

	@Value( "#{ configuracionProperties['flickr.api.metodo.detalle'] }" )
	private String metodoDetalle;

	@Override
	public List<Fotografia> buscarFotografias( String palabra ) {

		HttpClient client = new HttpClient();
		try {
			String respuestaJson = buscarFlickr( palabra, client );
			List<Fotografia> fotografias = extraerFotografias( respuestaJson, client );
			return fotografias;
		} catch ( Exception e ) {
			log.error( "Error al buscar imagenes en Flickr: {}", e.getMessage() );
		}
		return new ArrayList<Fotografia>();
	}

	private String buscarFlickr( String palabra, HttpClient httpClient ) {
		HttpMethod method = new GetMethod( apiUrl );
		try {
			method.setQueryString(
					new NameValuePair[]{
						new NameValuePair( "key", "value" ),
						new NameValuePair( "method", metodoBusqueda ), new NameValuePair( "api_key", apiKey ),
						new NameValuePair( "text", palabra ), new NameValuePair( "format", "json" ),
						new NameValuePair( "nojsoncallback", "1" ),
						new NameValuePair( "per_page", "10" )
					}
			);

			httpClient.executeMethod( method );
			byte[] response = method.getResponseBody();
			String jsonResponse = new String( response, "UTF-8" );
			log.debug( "Respuesta de Flickr: \n{}", jsonResponse );
			return jsonResponse;
		} catch ( Exception e ) {
			log.error( "Error al buscar imagenes en Flickr: {}", e.getMessage() );
		} finally {
			method.releaseConnection();
		}
		return null;
	}

	private List<Fotografia> extraerFotografias( String json, HttpClient cliente ) {
		List<Fotografia> fotografias = new ArrayList<Fotografia>();
		try {
			ObjectMapper mapper = new ObjectMapper();
			JsonNode rootNode = mapper.readTree( json );
			JsonNode photos = rootNode.get( "photos" ).get( "photo" );
			Iterator<JsonNode> iterador = photos.elements();
			while( iterador.hasNext() ) {
				JsonNode nodo = iterador.next();
				String id = nodo.get( "id" ).asText();
				String url = urlFotografia( id, cliente );

				Fotografia fotografia = new Fotografia();
				fotografia.setId( id );
				fotografia.setUrl( url );
				fotografia.setTitulo( nodo.get( "title" ).asText() );
				fotografia.setSecreto( nodo.get( "secret" ).asText() );
				fotografias.add( fotografia );
			}
		} catch( Exception e ) {
			log.error( "Error al extraer imagenes de la respuesta de Flickr: {}", e.getMessage() );
		}
		return fotografias;
	}

	private String urlFotografia( String id, HttpClient cliente ) {
		HttpMethod method = new GetMethod( apiUrl );
		try {
			method.setQueryString(
					new NameValuePair[]{
							new NameValuePair( "method", metodoDetalle ),
							new NameValuePair( "api_key", apiKey ),
							new NameValuePair( "photo_id", id ),
							new NameValuePair( "format", "json" ),
							new NameValuePair( "nojsoncallback", "1" )
					}
			);
			cliente.executeMethod( method );
			byte[] response = method.getResponseBody();
			String json = new String( response, "UTF-8" );

			ObjectMapper mapper = new ObjectMapper();
			JsonNode rootNode = mapper.readTree( json );
			JsonNode tamanyos = rootNode.get( "sizes" ).get( "size" );

			return tamanyos.get( 0 ).get( "source" ).asText();
		} catch ( Exception e ) {
			log.error( "Error al buscar url imagen en Flickr: {}", e.getMessage() );
		} finally {
			method.releaseConnection();
		}
		return null;
	}
}
