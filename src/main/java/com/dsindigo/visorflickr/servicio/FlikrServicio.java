package com.dsindigo.visorflickr.servicio;

import com.dsindigo.visorflickr.modelo.Fotografia;

import java.util.List;

public interface FlikrServicio {

	List<Fotografia> buscarFotografias( String palabra );

}
